package com.attinad.cre.graph.relationshipEntity;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import com.attinad.cre.graph.entity.Content;
import com.attinad.cre.graph.entity.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
@RelationshipEntity(type = "USER_RATING")
public class UserRating {

	@GraphId
	private Long id;
	
	@StartNode
	private User user;
	
	@EndNode
	private Content content;
	
	float rating;
	String comment;
	
	public UserRating(){
		
	}
	public UserRating(User user, Content content,float rating,String comment){
		this.user = user;
		this.content = content;
		this.rating = rating;
		this.comment = comment;
		updateContenRating(content, rating);
	}
	public Long getId() {
		return id;
	}
	public User getUser() {
		return user;
	}
	public Content getContent() {
		return content;
	}
	public float getRating() {
		return rating;
	}
	public String getComment() {
		return comment;
	}
	
	public void updateContenRating(Content content,float rating){
		content.setUserRatingInAVG(findAverageOf(content.getUserRatingInAVG(),rating));
	}
	
	public float findAverageOf(float contentRating, float userRating){
		return (contentRating+userRating)/2;
	}
	
}
