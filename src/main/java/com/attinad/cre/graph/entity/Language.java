package com.attinad.cre.graph.entity;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
public class Language {

	@GraphId Long id;
	
	String languagekey;
	String languageName;
	
	
}
