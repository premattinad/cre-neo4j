package com.attinad.cre.graph.entity;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
public class Device {

	@GraphId Long id;
	
	String name;
	String brandName;
	String modelNumber;
	
}
