package com.attinad.cre.graph.entity;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Location {

	@GraphId Long id;
	
	String name;
	String Latitude;
	String Longitude;
	
	@Relationship(type="MOTHER_LANGUAGE", direction = Relationship.OUTGOING)
	Language motherLanguage;
}
