package com.attinad.cre.graph.entity;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class CellularCarrier {

	@GraphId Long id;
	
	String name;
	String type;
	
	@Relationship(type="CONNECTION_SPEED", direction = Relationship.OUTGOING)
	Set<ConnectionSpeed> connectionSpeed = new HashSet<>();
}
