package com.attinad.cre.repositories;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.attinad.cre.mongo.entity.Event;
import com.attinad.cre.util.MongoUtil;

/**
 * The Class PagingSupportRepository.
 */

public class PagingSupportRepository {

	private PagingSupportRepository(){
		
	}
	/**
	 * @return
	 */
	public static List<Event> getEventListByEventTypeNotEquel(String eventType) {
		Query searchUserQuery = new Query(Criteria.where("event").ne(eventType));
		return MongoUtil.mongoOperation.find(searchUserQuery, Event.class);
	}
	/**
	 * @param eventType
	 * @return
	 */
	public static List<Event> getEventListByEventTypeEquel(String eventType) {
		Query searchUserQuery = new Query(Criteria.where("event").is(eventType));
		return MongoUtil.mongoOperation.find(searchUserQuery, Event.class);
	}

}
