package com.attinad.cre.mongo.service;

import org.springframework.stereotype.Service;

@Service
public interface EventProcessingService {

	public void processContent();
}
