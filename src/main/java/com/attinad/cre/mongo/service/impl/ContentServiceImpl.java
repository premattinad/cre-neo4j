package com.attinad.cre.mongo.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.attinad.cre.graph.entity.Content;
import com.attinad.cre.mongo.entity.Event;
import com.attinad.cre.mongo.service.ContentService;
import com.attinad.cre.repositories.ContentRepository;
import com.attinad.cre.util.MongoUtil;

/**
 * Implementation of ContentService.
 */
@Service
@Repository
public class ContentServiceImpl implements ContentService {

	@Autowired
	ContentRepository contentRepository;
	

	/**
	 * Content process.
	 * 
	 * @return no response
	 */
	@Override
	public void processContent() {
		
		Query searchUserQuery = new Query(Criteria.where("event").ne("Identify"));
		List<Event> eventList = MongoUtil.mongoOperation.find(searchUserQuery, Event.class);
		for (Event event : eventList) {
			System.out.println(event.getEvent());
		}
	}


	/**
	 * @param contents
	 */
	private void saveContent(List<Content> contents) {
		try {
		for (Content content : contents) {
			if(contentRepository.findByContentId(content.getContentId()) == null){
				contentRepository.save(content);
			}
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}


}
