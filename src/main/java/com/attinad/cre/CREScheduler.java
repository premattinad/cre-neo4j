package com.attinad.cre;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.attinad.cre.graph.entity.Person;
import com.attinad.cre.graph.service.PersonService;
import com.attinad.cre.mongo.service.ContentService;

@Component
public class CREScheduler
{
	@Autowired
	private PersonService personService;
	@Autowired
	private ContentService contentService;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	@Scheduled(cron = "${cron.expression}")
    public void demoServiceMethod()
    {
		contentService.processContent();
//		System.out.println("The time is now {}"+ dateFormat.format(new Date()));
//		Product product = productService.add();
//		Person perosn = personService.savePerson(new Person("James"));
//		System.out.println("product id - "+perosn.getId()+"    name - "+perosn.getName());
    }
//	@Scheduled(cron = "${cron.expression}")
//    public void demoServiceMethod()
//    {
//		System.out.println("The time is now {}"+ dateFormat.format(new Date()));
//		System.out.println("skip ------  "+ SharedClass.getSharedObject().skip);
//		schedulerService.processEvents(SharedClass.getSharedObject().limit, SharedClass.getSharedObject().skip);
////		System.getProperties().put("proxySet", "true");
//    }
}